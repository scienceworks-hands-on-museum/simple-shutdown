
import logging
from simpleshutdown.ClientNode import ClientNode
import requests
from requests.exceptions import ConnectTimeout, ConnectionError
import time
import traceback

def tryGetSchedule(client):
    try:
        client.tryGetSchedule()
    except (ConnectTimeout, ConnectionError):
        print('Failed to connect, check config & networking.')
    except Exception as e:
        print('Unexpected exception trying to get schedule.')
        logging.error(traceback.format_exc())

def main():
    '''
    Ping main node periodically.
    '''

    client = ClientNode()
    pingT = time.time()
    updateT = time.time()

    if not client.mainNodeConfigured():
        print('Node not configured, run scripts/init.py.')
        
        return

    tryGetSchedule(client)

    while True:
        t = time.time()

        if t - pingT >= ClientNode.PING_INTERVAL_SECONDS:
            pingT = t

            tryGetSchedule(client)

        if t - updateT >= ClientNode.UPDATE_INTERVAL_SECONDS:
            updateT = t

            client.tryUpdate()

        client.doSchedule()

if __name__ == '__main__':
    main()
