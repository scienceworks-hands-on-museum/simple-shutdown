from pythonlambdaserver.LambdaService import LambdaService
from pythonlambdaserver.LambdaServiceMethodConfig import LambdaServiceMethodConfig

class SimpleShutdownService(LambdaService):
    '''
    TODO.
    '''

    METHOD_CONFIGURATION = {
        'default': {
            'getSchedule': LambdaServiceMethodConfig(True),
            'listClientNodes': LambdaServiceMethodConfig(True),
            'setShutdownTime': LambdaServiceMethodConfig(True),
            'registerClientNode': LambdaServiceMethodConfig(True),
            'deregisterClientNode': LambdaServiceMethodConfig(True),
        }
    }

    @property
    def default(self):
        from simpleshutdown.SimpleShutdown import SimpleShutdown

        return SimpleShutdown()

def lambda_handler(event, context):
    return SimpleShutdownService.handleEvent(event)