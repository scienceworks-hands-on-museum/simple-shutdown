#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(name='museum-facility-automation',
    version='1.0',
    description='',
    author='John D. Sutton',
    author_email='',
    url='',
    packages=find_packages(),
    install_requires=[
        'nose2',
        'coverage',
        'sphinx',
        'getmac',
        'google-api-python-client',
        'google-auth-oauthlib',
        'pytz',
        'tzlocal',
        'iso8601',
        'wakeonlan',
        'pythonlambdaserver @ git+https://gitlab.com/jdsutton/PythonLambdaServer',
        #'eekquery @ git+https://gitlab.com/jdsutton/EekQuery',
        'slurp @ git+https://gitlab.com/jdsutton/Slurp',
    ],
)
