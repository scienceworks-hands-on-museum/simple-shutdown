import wakeonlan
from .SimpleShutdown import SimpleShutdown

class Startupper:
    '''
    Sends wake-on-LAN packet to all client nodes.
    '''

    @staticmethod
    def startup():
        clients = SimpleShutdown().listClientNodes()

        for _, client in clients.items():
            mac = client.get('mac', None)

            if mac:
                wakeonlan.send_magic_packet(mac)
