
from datetime import datetime
import json
from .Schedule import Schedule

class SimpleShutdown:
    '''
    Handles scheduled shutdowns.
    '''

    _CLIENT_DATA_FILE = 'client-data.json'
    
    def _getClients(self):
        try:
            with open(self.__class__._CLIENT_DATA_FILE, 'r') as f:
                return json.loads(f.read())
        except FileNotFoundError:
            return {}

    def _writeClients(self, clients):
        with open(self.__class__._CLIENT_DATA_FILE, 'w') as f:
            f.write(json.dumps(clients))
        
    def _bumpClientNodeHealth(self, nodeName, mac):
        clients = self._getClients()

        clients[nodeName] = {
            'lastUpdated': str(datetime.now()),
            'mac': mac,
        }

        self._writeClients(clients)

    def registerClientNode(self, nodeName, mac=None):
        self._bumpClientNodeHealth(nodeName, mac)

        return True

    def deregisterClientNode(self, nodeName):
        clients = self._getClients()

        clients.pop(nodeName, None)

        self._writeClients(clients)

    def listClientNodes(self):
        return self._getClients()

    def getSchedule(self, nodeName=None, mac=None):
        if nodeName is not None:
            self._bumpClientNodeHealth(nodeName, mac)
        
        return Schedule.getSchedule()

    def setShutdownTime(self, date, time):
        Schedule.writeException(date, time)
