
from datetime import datetime, timedelta
import json
import pytz
from .SimpleShutdownConfigFactory import SimpleShutdownConfigFactory
import time

class Schedule:

    DATE_FORMAT = '%Y-%m-%d'
    _SCHEDULE_PATH = 'schedule.json'
    _SCHEDULE_N_DAYS = 14
    _WINDOW_MINUTES = 1
    _STARTUP_WINDOW_MINUTES = 15

    @classmethod
    def localnow(cls):
        config = SimpleShutdownConfigFactory.produce()

        timezone = pytz.timezone(config.timezone)

        return datetime.now(timezone)

    @classmethod
    def _writeSchedule(cls, schedule):
        with open(cls._SCHEDULE_PATH, 'w') as f:
            f.write(json.dumps(schedule, indent=2))

    @classmethod
    def _getSchedule(cls):
        '''
        Return the schedule for the next N days, including today.
        '''

        try:
            with open(cls._SCHEDULE_PATH, 'r') as f:
                schedule = json.loads(f.read())
        except (FileNotFoundError, json.decoder.JSONDecodeError):
            print('Warning: Schedule not found')
            schedule = {
                'lastUpdatedTime': time.time() * 1000,
                'schedule': {},
                'startupSchedule': {},
                'eventSchedule': {}
            }

            cls._writeSchedule(schedule)

        if not schedule.get('schedule', None):
            schedule['schedule'] = {}

        if not schedule.get('startupSchedule', None):
            schedule['startupSchedule'] = {}

        if not schedule.get('eventSchedule', None):
            schedule['eventSchedule'] = {}
    
        return schedule

    @classmethod
    def getSchedule(cls):
        schedule = cls._getSchedule()

        # Populate schedule.
        config = SimpleShutdownConfigFactory.produce()
        day = cls.localnow()
        oneDay = timedelta(days=1)
        baseSchedule = config.get('shutdownSchedule', [])
        eventSchedule = schedule.get('eventSchedule', {})
        baseStartupSchedule = config.get('startupSchedule', None)
        defaultStartup = config.get('startupTime', '9:00')

        for i in range(cls._SCHEDULE_N_DAYS):
            d = day.strftime(cls.DATE_FORMAT)
            dayOfWeek = day.weekday()

            baseCloseTime = baseSchedule[dayOfWeek]
            eventCloseTime = schedule['eventSchedule'].get(d, '00:00')
            userCloseTime = schedule['schedule'].get(d, None)

            if userCloseTime is not None:
                # User override base value.
                baseCloseTime = userCloseTime

            closeTime = max(baseCloseTime, eventCloseTime)

            schedule['schedule'][d] = closeTime

            if baseStartupSchedule:
                schedule['startupSchedule'][d] = baseStartupSchedule[dayOfWeek]
            else:
                schedule['startupSchedule'][d] = defaultStartup

            day += oneDay

        return schedule
    
    @classmethod
    def writeException(cls, date, t, *, key='schedule'):
        schedule = cls._getSchedule()

        today = datetime.fromisoformat(cls.localnow().strftime(cls.DATE_FORMAT))

        if not key in schedule:
            schedule[key] = {}

        # Clear past dates.
        keys = list(schedule[key].keys())
        for k in keys:
            dt = datetime.fromisoformat(k)

            if dt < today:
                del schedule[key][k]

        schedule[key][date] = t
        schedule['lastUpdatedTime'] = time.time() * 1000

        cls._writeSchedule(schedule)

    @classmethod
    def writeEventException(cls, date, t):
        cls.writeException(date, t, key='eventSchedule')    

    @classmethod
    def getTodayDate(cls):
        return cls.localnow().strftime(cls.DATE_FORMAT)

    @classmethod
    def getTodaysStartupTime(cls):
        date = cls.getTodayDate()

        return cls.getSchedule()['startupSchedule'][date]

    @classmethod
    def getTodaysShutdownTime(cls):
        date = cls.getTodayDate()

        return cls.getSchedule()['schedule'][date]

    @classmethod
    def getStartupWindow(cls):
        t = cls.getTodaysStartupTime()

        hour = int(t.split(':')[0])
        minute = int(t.split(':')[1])

        now = cls.localnow()
        startOfWindow = now.replace(hour=hour, minute=minute)
        endOfWindow = startOfWindow + timedelta(minutes=cls._WINDOW_MINUTES)

        return (startOfWindow, endOfWindow)

    @classmethod
    def getShutdownWindow(cls):
        t = cls.getTodaysStartupTime()

        hour = int(t.split(':')[0])
        minute = int(t.split(':')[1])

        now = cls.localnow()
        startOfWindow = now.replace(hour=hour, minute=minute)
        endOfWindow = startOfWindow + timedelta(minutes=cls._STARTUP_WINDOW_MINUTES)

        return (startOfWindow, endOfWindow)
