
import subprocess
import sys

class Shutdowner:
    '''
    Shuts down the computer.
    '''

    _shutdownAlready = False

    @classmethod
    def shutdown(cls):
        if cls._shutdownAlready:
            return

        cls._shutdownAlready = True
        
        if 'win' in sys.platform:
            subprocess.run('shutdown /s'.split())
        else:
            subprocess.run('systemctl poweroff'.split())

