
import logging
import traceback
import subprocess

class Updater:
    '''
    Updates & installs the repository.
    '''

    @staticmethod
    def _tryExecute(command):
        try:
            result = subprocess.run(
                command.split(),
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
            ).stderr

            result = result.decode('utf-8')

            if len(result) > 0:
                logging.error(result)
                logging.error('Failed to `' + command + '`.')

                return False

        except Exception as e:
            logging.error(traceback.format_exc())
            logging.error('Failed to `' + command + '`.')

            return False

        return True

    @classmethod
    def tryUpdate(cls):
        '''
        Try to pull and install.
        '''

        if not cls._tryExecute('git pull --rebase'):
            return

        cls._tryExecute('pip install --upgrade .')
