from google.oauth2.credentials import Credentials
from googleapiclient.errors import HttpError
from googleapiclient.discovery import build
from datetime import datetime, time
import logging, traceback
import iso8601
import tzlocal
from .SimpleShutdownConfigFactory import SimpleShutdownConfigFactory

class CalendarReader:

    def __init__(self, credentials):
        '''
        Initializes a CalendarReader object.

        Args:
            credentials (google.oauth2.credentials.Credentials): The user credentials for accessing the Google Calendar API.
        '''
        self.service = build('calendar', 'v3', credentials=credentials)

    def getTodayEvents(self, calendarIds):
        events = []

        config = SimpleShutdownConfigFactory.produce()
        timezone = tzlocal.get_localzone()
        today = datetime.now(timezone).date()

        startOfToday = datetime.combine(today, time.min).astimezone().isoformat()
        endOfToday = datetime.combine(today, time.max).astimezone().isoformat() 

        for calendarId in calendarIds:
            try:
                eventsResult = self.service.events().list(
                    calendarId=calendarId,
                    timeMin=startOfToday,
                    timeMax=endOfToday,
                    singleEvents=True,
                    orderBy='startTime'
                ).execute()

                es = eventsResult.get('items', [])

                es = filter(lambda x: x.get('date', None) is None, es)
                events += list(es)
            except Exception as error:
                logging.error(traceback.format_exc())

        return events

    def getTimeRange(self, events):
        '''
        Assumes events are for same day.
        '''

        if not events:
            return (None, None)

        events = filter(lambda x: x['start'].get('dateTime', False) and x['end'].get('dateTime', False), events)
        events = list(events)

        if not events:
            return (None, None)

        localZone = tzlocal.get_localzone()

        def getDT(x):
            dt = iso8601.parse_date(x.get('dateTime'))

            return dt.astimezone(localZone)

        start = min(map(lambda x: getDT(x['start']), events))
        end = max(map(lambda x: getDT(x['end']), events))

        return (start, end)
