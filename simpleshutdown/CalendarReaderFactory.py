from google.auth.transport.requests import Request
from google.oauth2.credentials import Credentials
from google.oauth2 import service_account
from google_auth_oauthlib.flow import InstalledAppFlow

from .CalendarReader import CalendarReader

class CalendarReaderFactory:

    @staticmethod
    def _getCredentials():
        SCOPES = ['https://www.googleapis.com/auth/calendar.readonly']

        creds = None
        
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                creds = service_account.Credentials.from_service_account_file('credentials.json', scopes=SCOPES)

        return creds

    @classmethod
    def produce(cls):
        return CalendarReader(cls._getCredentials())
