
from eekquery.ConfigFactory import ConfigFactory
import json
import os

class SimpleShutdownConfigFactory(ConfigFactory):
    '''
    Produces configuration data as EekQuery.DataObject.DataObject.
    '''

    _CONFIG_FILE = os.path.join('config', 'simpleshutdown.config.json')

    @classmethod
    def produce(cls):
        return super().produce(SimpleShutdownConfigFactory._CONFIG_FILE)
