
from datetime import datetime, timedelta
from eekquery.ConfigFactory import ConfigFactory
import getmac
import json
import logging
from pythonlambdaserver.LambdaRequestSender import LambdaRequestSender
# import socket
from .Schedule import Schedule
from .SimpleShutdownConfigFactory import SimpleShutdownConfigFactory
from .Shutdowner import Shutdowner
from .Updater import Updater

class ClientNode:
    '''
    Client node for remote control.
    '''

    UPDATE_INTERVAL_SECONDS = 60 * 60 * 24
    PING_INTERVAL_SECONDS = 60 * 5

    class MainNodeNotConfiguredError(RuntimeError):
        pass

    def __init__(self,
        config=SimpleShutdownConfigFactory.produce(),
        shutdowner=Shutdowner,
        dt=datetime,
        schedule=Schedule,
        updater=Updater):

        self._config = config
        self._datetime = dt
        self._shutdowner = shutdowner
        self._schedule = schedule
        self._updater = updater
        self.name = config.nodeName
        self._mainNodeIP = config.get('mainNodeIP', None)

    def mainNodeConfigured(self):
        return self._mainNodeIP is not None

    def tryGetSchedule(self):
        '''
        Try to fetch the schedule from the main node.
        '''

        if self._mainNodeIP is None:
            raise ClientNode.MainNodeNotConfiguredError('Must call setMainNodeIP() first')

        # localIp = socket.gethostbyname(socket.gethostname())

        result = LambdaRequestSender.sendRequest(
            self._mainNodeIP,
            'SimpleShutdownService',
            'default.getSchedule',
            [
                self._config.nodeName,
                getmac.get_mac_address(),
            ],
            {},
        )

        for date, time in result['schedule'].items():
            self._schedule.writeException(date, time)

    def doSchedule(self):
        '''
        Shutdown.
        '''

        now = self._schedule.localnow()

        start, end = self._schedule.getShutdownWindow()

        if now >= start and now <= end:
            self._shutdowner.shutdown()

    def tryUpdate(self):
        '''
        Try to update the program.
        '''
        
        self._updater.tryUpdate()
