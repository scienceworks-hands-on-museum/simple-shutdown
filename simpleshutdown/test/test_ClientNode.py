from ..SimpleShutdownConfigFactory import SimpleShutdownConfigFactory
SimpleShutdownConfigFactory._CONFIG_FILE = 'config/test.json'

from datetime import datetime
from eekquery.DataObject import DataObject
from simpleshutdown.ClientNode import ClientNode
import unittest
from unittest.mock import MagicMock

class FakeDT:
    hour = 17
    minute = 1

    @classmethod
    def now(cls):
        return datetime.now().replace(day=1, month=1, year=2023, hour=cls.hour, minute=cls.minute)

    @classmethod
    def atTime(cls, hour, minute):
        return datetime.now().replace(day=1, month=1, year=2023, hour=hour, minute=minute)

class MockSchedule:
    def getSchedule(self):
        return {
            'schedule': {
                '2023-01-01': '17:00',
            },
        }

    def localnow(self):
        return FakeDT.now()

    def getShutdownWindow(self):
        return (FakeDT.atTime(16, 59), FakeDT.atTime(17, 1))


def getSimple():
    config = DataObject({
        'mainNodeIP': '192.168.0.1',
        'isMainNode': True,
        'nodeName': 'Main Node',
        'shutdownSchedule': [
            '17:01',
            '17:01',
            '17:01',
            '17:01',
            '17:01',
            '17:01',
            '17:01',
        ]
    }, autoField=True)

    shutdowner = MagicMock()

    simple = ClientNode(
        config=config,
        shutdowner=shutdowner,
        dt=FakeDT,
        schedule=MockSchedule(),
    )

    return (simple, shutdowner)

class test_ClientNode(unittest.TestCase):

    def test_doSchedule(self):
        # Case 1.
        simple, shutdowner = getSimple()
        simple.doSchedule()

        self.assertTrue(shutdowner.shutdown.called)

        # Case 2.
        simple, shutdowner = getSimple()

        FakeDT.hour = 17
        FakeDT.minute = 32

        simple.doSchedule()

        self.assertFalse(shutdowner.shutdown.called)

        # Case 3.
        simple, shutdowner = getSimple()

        FakeDT.hour = 17
        FakeDT.minute = 0

        simple.doSchedule()

        self.assertTrue(shutdowner.shutdown.called)

    def test_getSchedule_doSchedule(self):
        config = DataObject({
            'mainNodeIP': '192.168.0.1',
            'isMainNode': True,
            'nodeName': 'Main Node',
            'shutdownSchedule': [
                '17:01',
                '17:01',
                '17:01',
                '17:01',
                '17:01',
                '17:01',
                '17:01',
            ]
        }, autoField=True)

        shutdowner = MagicMock()

        simple = ClientNode(
            config=config,
            shutdowner=shutdowner,
            dt=FakeDT,
            schedule=MockSchedule(),
        )

        # Case 1.
        FakeDT.hour = 17
        FakeDT.minute = 5
        simple.doSchedule()

        self.assertFalse(shutdowner.shutdown.called)

        # Case 2.
        FakeDT.hour = 17
        FakeDT.minute = 0

        simple.doSchedule()

        self.assertTrue(shutdowner.shutdown.called)

if __name__ == '__main__':
    unittest.main()
