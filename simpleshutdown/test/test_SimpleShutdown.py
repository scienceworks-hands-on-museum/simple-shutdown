
from ..SimpleShutdownConfigFactory import SimpleShutdownConfigFactory
SimpleShutdownConfigFactory._CONFIG_FILE = 'config/test.json'

from datetime import datetime
from eekquery.DataObject import DataObject
from simpleshutdown.SimpleShutdown import SimpleShutdown
import unittest

def getSimple():
    config = DataObject({
        'mainNodeIP': '192.168.0.1',
        'isMainNode': True,
        'nodeName': 'Main Node',
        'shutdownSchedule': [
            '17:01',
            '17:01',
            '17:01',
            '17:01',
            '17:01',
            '17:01',
            '17:01',
        ]
    }, autoField=True)


    simple = SimpleShutdown(
        config=config,
    )

    return simple

class test_SimpleShutdown(unittest.TestCase):

    pass

if __name__ == '__main__':
    unittest.main()
