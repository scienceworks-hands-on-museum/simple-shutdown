import unittest
import datetime
from unittest.mock import MagicMock, patch
from google.oauth2.credentials import Credentials
from googleapiclient.errors import HttpError
from ..CalendarReader import CalendarReader


class TestCalendarReader(unittest.TestCase):
    def setUp(self):
        self.credentials = MagicMock(spec=Credentials)
        self.calendarIds = ['primary']
        self.reader = CalendarReader(self.credentials)

    def test_get_today_events(self):
        events = [{'summary': 'Test event 1'}, {'summary': 'Test event 2'}, {'summary': 'Test event 3'}]
        service_mock = MagicMock()
        service_mock.events().list().execute.return_value = {'items': list(events)}

        with patch.object(self.reader, 'service', service_mock):
            today_events = self.reader.getTodayEvents(self.calendarIds)

            self.assertEqual(today_events, events)

    def test_get_time_range(self):
        import iso8601

        now = datetime.datetime.utcnow()
        time_min ='2023-03-27T01:00:00.0Z'
        time_max = '2023-03-27T18:00:00-07:00'

        events = [
            {'summary': 'Test event 1', 'start': {'dateTime': time_min}, 'end': {'dateTime': time_max}},
            {'summary': 'Test event 3', 'start': {'dateTime': time_min}, 'end': {'dateTime': time_max}}]
        service_mock = MagicMock()
        service_mock.events().list().execute.return_value = {'items': list(events)}
        
        expected_range = (time_min, time_max)
        
        with patch.object(self.reader, 'service', service_mock):
            e = self.reader.getTodayEvents(self.calendarIds)

        self.assertEqual(self.reader.getTimeRange(e)[0], iso8601.parse_date(expected_range[0]))
        self.assertEqual(self.reader.getTimeRange(e)[1], iso8601.parse_date(expected_range[1]))

if __name__ == '__main__':
    unittest.main()