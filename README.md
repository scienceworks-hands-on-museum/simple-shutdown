# Simple Shutdown
Automatically shut down computers across the network on a schedule.
Accept shutdown delays via a web interface.

## Getting Started

### Installation
```
git clone git@gitlab.com:scienceworks-hands-on-museum/simple-shutdown.git
cd simple-shutdown

sudo apt update
sudo apt install virtualenv build-essential

# Make sure python3 is installed on your system:
python3 --version

virtualenv -p python3 env
. env/bin/activate

make install
```

### Configuration
Edit config/simpleshutdown.config.json

### Run the webserver
```
# Make sure you have done . env/bin/activate first

make
```

Now you may open the page at http://127.0.0.1:5000