#!/usr/bin/python3

import os
import sys
sys.path.insert(0, os.path.abspath('..'))

from simpleshutdown.CalendarReaderFactory import *
from simpleshutdown.SimpleShutdownConfigFactory import *

reader = CalendarReaderFactory.produce()
config = SimpleShutdownConfigFactory.produce()

print(reader.getTodayEvents(config.calendarIds))
