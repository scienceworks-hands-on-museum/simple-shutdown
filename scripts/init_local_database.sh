#!/bin/bash

DATABASE=${1:-DefaultDB}
DIRECTORY=`dirname $0`

mysql -u root -p$MYSQL_PASSWORD -e "DROP DATABASE IF EXISTS $DATABASE; CREATE DATABASE $DATABASE;"
mysql -u root -p$MYSQL_PASSWORD $DATABASE < $DIRECTORY/../authentication/schema/schema.sql
mysql -u root -p$MYSQL_PASSWORD $DATABASE < $DIRECTORY/../schema/schema.sql
mysql -u root -p$MYSQL_PASSWORD -e "SET @@global.time_zone = '+00:00';"