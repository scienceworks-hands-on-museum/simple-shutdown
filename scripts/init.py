
import json

path = 'config/simpleshutdown.config.json'

try:
    with open(path, 'r') as f:
        pass
except FileNotFoundError:
    mainNodeIP = input('Enter main node ip (http://X.X.X.X:5001): ')
    nodeName = input('Enter the name for this node: ')

    data = {
        'frontend_dev': {
            'backendEndpoint': 'http://localhost:5001',
            'authenticationService': 'MyAppAuthenticationService'
        },
        'mainNodeIP': mainNodeIP,
        'nodeName': nodeName,
        'calendarIds': [],
        'timezone': 'US/Pacific',
        'shutdownSchedule': [
            '17:01',
            '17:01',
            '17:01',
            '17:01',
            '17:01',
            '17:01',
            '17:01',
        ]
    }

    with open(path, 'w') as f:
        f.write(json.dumps(data, indent=2))
