import('SimpleShutdownResponse.js');
import('/src/JSUtils/backend/BackendService.js');

const SimpleShutdown = BackendService.declare(
    'SimpleShutdownService',
    SimpleShutdownResponse,
    ).forAllRequests(() => {
        return {};
    });