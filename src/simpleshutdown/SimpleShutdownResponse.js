import('/src/JSUtils/util/DataObject.js');

/**
 * @class
 */
class SimpleShutdownResponse extends DataObject {
    
    /**
     * @public
     */
    init() {

    }

    /**
     * @public
     */
    static fromFormData(data) {
        let result = new SimpleShutdownResponse(data);


        return result;
    }

    /**
     * @public
     */
    toFormData() {
        let result = Object.assign({}, this);


        return result;
    }
}