import('/src/jsgame/Animation.js');
import('/src/jsgame/Game.js');
import('/src/jsgame/JSImage.js');
import('/src/jsgame/Prop.js');
import('/src/jsgame/Tileset.js');
import('ComputerProp.js');
import('DeskProp.js');
import('ScientistNPC.js');

class AutomationGame extends Game {

    static _TILESET_URI = '/assets/Modern_Office_Revamped/Modern_Office_48x48.png';
    static _OFFICE_URI = '/assets/Modern_Office_Revamped/1_Room_Builder_Office/Room_Builder_Office_48x48.png';
    static _SCIENTISTS_URI = '/assets/Scientists.png';
    static _TILE_SIZE = 48;

    /**
     * @override
     * @public
     */
    static init(nodes) {
        this._computers = [];
        this._schedule = schedule;

        Promise.all([
            JSImage.load(AutomationGame._TILESET_URI),
            JSImage.load(AutomationGame._OFFICE_URI),
            JSImage.load(AutomationGame._SCIENTISTS_URI),
            JSImage.load(ComputerProp.TILESET_URI),
        ]).then(() => {
            super.init(...arguments);

            this.graphics.scalePixelized();
            this.graphics.font('16px Pixelify Sans');
            this.graphics.textAlign(Shapes.ALIGN.CENTER);
            this.currentRoom.game = this;

            this._initTiles();
            this._initNPCs();
        });
    }

    /**
     * @public
     */
    static setNodes(nodes) {
        this._initDesks(nodes);

        return this;
    }

    /**
     * @public
     */
    static setSchedule(schedule) {
        this._schedule = schedule;

        for (let computer of this._computers) {
            computer.setSchedule(schedule);
        }

        return this;
    }

    /**
     * @override
     * @public
     */
    static draw() {
        this._graphics.background(0);

        super.draw();
    }

    /**
     * @private
     */
    static _initTiles() {
        const area = 32 * this._TILE_SIZE;
        let prop;

        Tileset.load(this._OFFICE_URI, this._TILE_SIZE).then((tileset) => {
            const tile = tileset.getTile(12, 8);

            for (let x = 0; x < area; x += this._TILE_SIZE) {
                for (let y = 0; y < area; y += this._TILE_SIZE) {
                    prop = new Prop(x, y, this._TILE_SIZE, this._TILE_SIZE);
                    prop.image = tile;

                    this.currentRoom.addProp(prop);
                }
            }
        });
    }

    /**
     * @private
     */
    static _initNPCs() {
        const rows = [0, 4, 8, 12];
        const numWorkFrames = [16, 9, 10, 23];

        Tileset.load(this._SCIENTISTS_URI, 32, 32).then((tileset) => {

            for (let i = 0; i < rows.length; i++) {
                const y = rows[i]
                const frame0 = tileset.getTile(0, y);
                const frame1 = tileset.getTile(1, y);
                const frame2 = tileset.getTile(2, y);
                const frame3 = tileset.getTile(3, y);

                const animation = new Animation(null, ScientistNPC._FRAME_RATE).setImages([frame0, frame1, frame2, frame3]);

                let workFrames = [];
                for (let j = 0; j < numWorkFrames[i]; j++) {
                    workFrames.push(tileset.getTile(j, y + 2));
                }

                const workAnimation = new Animation(null, ScientistNPC._FRAME_RATE).setImages(workFrames);

                const npc = new ScientistNPC(32, 32 + y * 10)
                    .setAnimation(animation)
                    .setIdleAnimation(animation)
                    .setWorkAnimation(workAnimation);

                this.currentRoom.addProp(npc, 2);
            }
            
        });
    }

    /**
     * @private
     */
    static _initDesks(nodes) {
        this._computers = [];

        const nodeNames = Array.from(Object.keys(nodes));
        nodes = Array.from(Object.values(nodes));

        const deskX = 342;
        const deskY = 1371;
        const deskW = 122;
        const deskH = 69;
        const deskXSpace = deskW + 22;
        const deskYSpace = deskH + 27;
        
        JSImage.load(this._TILESET_URI).then((officeImg) => {
            const desk0 = JSImage.crop(officeImg, deskX, deskY, deskW, deskH);
            const desk1 = JSImage.crop(officeImg, deskX + deskXSpace, deskY, deskW, deskH);
            const desk2 = JSImage.crop(officeImg, deskX - deskXSpace * 2, deskY + deskYSpace, deskW, deskH);
            const desk3 = JSImage.crop(officeImg, deskX - deskXSpace, deskY + deskYSpace, deskW, deskH);
            const desk4 = JSImage.crop(officeImg, deskX, deskY + deskYSpace, deskW, deskH);

            const desks = [desk0, desk1, desk2, desk3, desk4];

            let x = 48;
            let y = 48;

            for (let i = 0; i < nodeNames.length; i++) {
                const desk = desks[i % desks.length];

                const deskProp = new DeskProp(x, y)
                    .setImage(desk);
                this.currentRoom.addProp(deskProp);

                const computer = new ComputerProp(deskProp.x + deskProp.width / 2, deskProp.y)
                    .setNode(nodeNames[i], nodes[i])
                    .setSchedule(this._schedule);
                computer.x -= computer.width / 2;
                this.currentRoom.addProp(computer, 1);
                this._computers.push(computer)

                x += deskW + 48;

                if (x + deskW >= this.viewWidth) {
                    x = 48;
                    y += deskH + 48;
                }
            }
        });
    }
    
}