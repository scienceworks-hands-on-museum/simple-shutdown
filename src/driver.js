import('AutomationGame.js');
import('/src/JSUtils/util/Datetime.js');
import('/src/JSUtils/util/Driver.js');
import('/src/JSUtils/util/Page.js');
import('/src/simpleshutdown/SimpleShutdown.js');

/**
 * @class
 */
class HomepageViewDriver extends Driver {

    static _IMG_URI = '/assets';

    /**
     * @public
     */
    static init() {
        super.init();

        this._schedule = null;

        this._showSchedule().then((schedule) => {
            this._listNodes().then((nodes) => {
                AutomationGame
                    .setNodes(nodes)
                    .setSchedule(schedule);
            });
        });

        AutomationGame.init('game');
    }

    /**
     * @private
     */
    static _listNodes() {
        // Dom.setContents(ID.NODES, '');

        // let row = `<tr><th>Node</th><th>Last Healthy<th></tr>`;

        // Dom.appendTo(ID.NODES, row);

        return SimpleShutdown.listClientNodes().then((nodes) => {
            // Object.keys(nodes).forEach((name) => {
            //     let date = new Date(nodes[name]['lastUpdated']);

            //     const time = Datetime.dateToTimeString(date);
            //     const timeSinceHealthyHours = (new Date() - date) / (60 * 60 * 1000);
            //     let c = 'class="Healthy"';
            //     let img = HomepageViewDriver._IMG_URI + '/computer-face/happy/1.png';

            //     if (timeSinceHealthyHours > this._NODE_HEALTHY_TIME_HOURS) {
            //         c = 'class="Unhealthy"';
            //         img = HomepageViewDriver._IMG_URI + '/computer-face/mad/3.png';
            //     } else if (nodes[name] < this._schedule.lastUpdatedTime) {
            //         c = 'class="Pending"';
            //         img = HomepageViewDriver._IMG_URI + '/computer-face/mad/3.png';
            //     }

            //     date = Datetime.dateToDateString2(date);
            //     row = `<tr ${c}>
            //         <td>${name}</td>
            //         <td>${time}, ${date}</td>
            //         <td><img class="Face" src="${img}">
            //         <td><button class="Button" onclick="Driver.getViewDriver().onSettingsButtonClicked('${name}');"><span class="material-symbols-outlined">settings</span></button></td>
            //     </tr>`;
                
            //     Dom.appendTo(ID.NODES, row);
            // });

            return nodes;
        });
    }

    /**
     * @private
     */
    static refreshNodes() {
        this._listNodes();
    }

    /**
     * @public
     */
    static onSettingsButtonClicked(nodeName) {
        const component = new NodeSettingsModalComponent(nodeName);

        Dom.appendTo(document.body, component.render());
    }

    /**
     * @private
     */
    static _showSchedule() {
        Dom.setContents(ID.SCHEDULE, '');

        let row = `<tr><th>Date</th><th>Startup</th><th>Shutdown<th></tr>`;

        Dom.appendTo(ID.SCHEDULE, row);

        return SimpleShutdown.getSchedule(null, {}).then((schedule) => {
            this._schedule = schedule;
            schedule = schedule.schedule;

            const startOfToday = new Date().setHours(0, 0, 0, 0);

            Array.from(Object.keys(schedule)).sort().forEach((key) => {
                const date = new Date(key);
                const dateUTC = Datetime.addTimezoneOffset(date);

                if (dateUTC.setHours(0, 0, 0, 0) < startOfToday) return;

                const day = Datetime.dateToDateString2(dateUTC);
                const dayName = dateUTC.toLocaleDateString(navigator.language, {weekday: 'long'});
                const startup = this._schedule.startupSchedule[key];
                const value = schedule[key];
                let c = '';

                if (Datetime.isToday(dateUTC)) {
                    c = 'class="Today"';
                }

                row = `<tr ${c}><td>${dayName}, ${day}</td><td>${startup}</td><td>${value}</td></tr>`;

                Dom.appendTo(ID.SCHEDULE, row);

                return schedule;
            });

            return Promise.resolve(schedule);
        });
    }

    /**
     * @private
     */
    static setDelay() {
        const data = Dom.getDataFromForm(ID.DATETIME);

        if (!data.date || !data.time) return;

        SimpleShutdown.setShutdownTime(data.date, data.time, {}).then(() => {
            this._showSchedule();
        });
    }
}

HomepageViewDriver._NODE_HEALTHY_TIME_HOURS = 1;

Page.addLoadEvent(() => {
    HomepageViewDriver.init();
});

const ID = {
    DATETIME: 'datetime',
    NODES: 'nodes',
    SCHEDULE: 'schedule',
    TEMPLATE: {
        
    }
};

const CLASS = {

};
