import('/src/jsgame/NPC.js');
import('/src/JSUtils/math/Random.js');
import('/src/JSUtils/math/Vector.js');

class ScientistNPC extends NPC {

    /**
     * @public
     * @override
     */
    constructor() {
        super(...arguments);

        this.width = 32 * 3;
        this._scale = 3;
        this._state = ScientistNPC._STATE.MOVE;
        this._target = null;
        this._idleAnimation = null;
        this._workAnimation = null;
    }

    /**
     * @public
     */
    setIdleAnimation(animation) {
        this._idleAnimation = animation;

        return this;
    }

    /**
     * @public
     */
    setWorkAnimation(animation) {
        this._workAnimation = animation;

        return this;
    }

    /**
     * @private
     */
    _move() {
        this._state = this.constructor._STATE.MOVE;
        this._target = null;
        this.setAnimation(this._idleAnimation);
    }

    /**
     * @private
     */
    _findDesk() {
        const desks = this.game.currentRoom.props.filter((x) => x instanceof DeskProp);
        
        const desk = Random.choice(Array.from(desks));

        if (!desk) return;

        this._target = {
            x: desk.x + Math.random() * (desk.width - this.width),
            y: desk.y,
        }
    }

    /**
     * @private
     */
    _work() {
        this._state = this.constructor._STATE.WORK;
        this.setAnimation(this._workAnimation);

        const t = this.constructor._WORK_TIME_MS;
        setTimeout(() => {
            this._move();
        }, t / 2 + Math.random() * t / 2);
    }

    /**
     * @public
     * @override
     */
    draw() {
        // Find an empty desk.
        if (this._state === this.constructor._STATE.MOVE) {
            if (!this._target) {
                this._findDesk();
            } else {
                // Move to the desk.
                this._moveTowardsTarget();
            }
        }

        // Try work.
        else if (this._state === this.constructor._STATE.WORK) {
            
        }

        // If fail, sad.
        // If succeed, happy.
        // Wait.

        super.draw();
    }

    /**
     * @private
     */
    _moveTowardsTarget() {
        let toTarget = new Vector(this._target.x - this.x, this._target.y - this.y);
            
        if (toTarget.magnitude() <= this.constructor._MOVE_SPEED) {
            this._x = this._target.x;
            this._y = this._target.y;

            this._work();

            return;
        }

        toTarget = toTarget.normalize()
            .multiply(this.constructor._MOVE_SPEED);

        this._x += toTarget.x;
        this._y += toTarget.y;
    }
}

ScientistNPC._GRAVITY = 0;
ScientistNPC._MOVE_SPEED = 2;
ScientistNPC._FRAME_RATE = 4;
ScientistNPC._STATE = {
    MOVE: 0,
    WORK: 1,
    SAD: 2,
    HAPPY: 3
};
ScientistNPC._WORK_TIME_MS = 1000 * 3;
