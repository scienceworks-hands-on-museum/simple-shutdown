import('/src/JSUtils/util/Datetime.js');
import('/src/jsgame/Prop.js');

class ComputerProp extends Prop {

    /**
     * @constructor
     * @override
     */
    constructor() {
        super(...arguments);

        this._img = ComputerProp._IMAGES.HAPPY;

        JSImage.load(this.constructor.TILESET_URI).then((tileset) => {
            this.setImage(this._img);
        });

        this.width = this.constructor._W;
        this.height = this.constructor._H;

        this._name = 'Node';
        this._node = null;
        this._schedule = null;
    }

    /**
     * @private
     */
    _updateImage() {
        if (!this._node || !this._schedule) return;

        const now = new Date();
        const nowDate = ('0' + now.getDate()).slice(-2);
        const today = now.getFullYear() + '-' + (now.getMonth() + 1) + '-' + nowDate;
        const lastUpdated = new Date(this._node.lastUpdated);
        const t = now - lastUpdated.getTime();

        const shutdown = this._schedule[today];
        const hours = parseInt(shutdown.split(':')[0]);
        const minutes = parseInt(shutdown.split(':')[1]);
        
        let shutdownTime = new Date();
        shutdownTime.setHours(hours);
        shutdownTime.setMinutes(minutes);

        if (t > this.constructor._TIME_TO_GRUMPY_MS) {
            if (now > shutdownTime && Datetime.isToday(lastUpdated)) {
                this._img = ComputerProp._IMAGES.SLEEPY;
                this.setImage(this._img);
            } else {
                this._img = ComputerProp._IMAGES.MAD;
            }

            this.setImage(this._img);

        }
    }

    /**
     * @public
     */
    setNode(name, node) {
        this._name = name;
        this._node = node;

        this._updateImage();

        return this;
    }

    /**
     * @public
     */
    setSchedule(schedule) {
        this._schedule = schedule;

        this._updateImage();

        return this;
    }

    /**
     * @public
     * @override
     */
    draw() {
        super.draw();

        let checkIn = new Date(this._node.lastUpdated);
        checkIn = Datetime.dateToDateTimeString(checkIn);

        this.game.graphics.fill(255);
        this.game.graphics.text(this._name, this.x + this.width / 2, this.y - 8);
        this.game.graphics.text(checkIn, this.x + this.width / 2, this.y + this.height + 8);
    }
}

ComputerProp.TILESET_URI = '/assets/computer-faces-tileset.png';
ComputerProp._W = 48;
ComputerProp._H = 48;
ComputerProp._TIME_TO_GRUMPY_MS = 1000 * 60 * 60;
ComputerProp._SLEEP_TIME_MS = 1000 * 60 * 60 * 8;

JSImage.load(ComputerProp.TILESET_URI ).then((tileset) => {
    ComputerProp._IMAGES = {
        HAPPY: JSImage.crop(tileset, 0, 0, ComputerProp._W, ComputerProp._H),
        MAD: JSImage.crop(tileset, 0, ComputerProp._H, ComputerProp._W, ComputerProp._H),
        SLEEPY: JSImage.crop(tileset, 0, ComputerProp._H * 2, ComputerProp._W, ComputerProp._H),
    };
});
