import('/src/jsgame/Prop.js');

class DeskProp extends Prop {

    /**
     * @constructor
     * @override
     */
    constructor() {
        super(...arguments);
        
        this.width = 122;
        this.height = 69;
    }
}