import('/src/JSUtils/components/ModalComponent/driver.js');
import('/src/simpleshutdown/SimpleShutdown.js');

/**
 * @class
 */
class NodeSettingsModalComponent extends ModalComponent {

    /**
     * @constructor
     */
    constructor(nodeName) {
        const params = {};
        
        super(NodeSettingsModalComponent.ID.TEMPLATE.THIS, params);

        this._nodeName = nodeName;
    }

    /**
     * @public
     */
    deleteNode() {
        SimpleShutdown.deregisterClientNode(this._nodeName, {}).then(() => {
            this.close();
        });

        Driver.getViewDriver().refreshNodes();
    }
}

NodeSettingsModalComponent.ID = {
    TEMPLATE: {
        THIS: 'NodeSettingsModalComponent_template',
    },
};
