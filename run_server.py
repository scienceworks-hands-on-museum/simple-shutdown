#!/usr/bin/env python3

import atexit
from pythonlambdaserver.LambdaServer import LambdaServerApp

def close():
    LambdaServerApp.close()

atexit.register(close)

def main():
    '''
    TODO.
    '''


    directory = './services'
    port = 5001

    LambdaServerApp.run(
        [directory],
        port,
        allowedOrigins=[
            'http://192.168.67.206:5000',
            'http://localhost:5000',
            'http://127.0.0.1:5000',
        ])

if __name__ == '__main__':
    main()
