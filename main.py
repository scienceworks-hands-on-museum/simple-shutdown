#!/usr/bin/env python3

import atexit
from datetime import datetime, timezone
import os
import json
from simpleshutdown.CalendarReaderFactory import CalendarReaderFactory
from simpleshutdown.Schedule import Schedule
from simpleshutdown.SimpleShutdownConfigFactory import SimpleShutdownConfigFactory
from simpleshutdown.Startupper import Startupper
import subprocess
import time

childProcess = None

def cleanUp():
    print('Closing...')

    if childProcess is not None:
        childProcess.exit()

atexit.register(cleanUp)

def tryStartup():
    now = Schedule.localnow()

    start, end = Schedule.getStartupWindow()

    if now >= start and now <= end:
        Startupper.startup()

def main():
    '''
    TODO.
    '''

    config = SimpleShutdownConfigFactory.produce()

    REFRESH_INTERVAL_SECONDS = 60 * 15

    calendarIds = config.calendarIds

    calendarReader = CalendarReaderFactory.produce()

    childProcess = subprocess.Popen('./run_server.py &', shell=True)

    while True:
        tryStartup()

        events = calendarReader.getTodayEvents(calendarIds)

        if events is None:
            continue

        todayEventTime = calendarReader.getTimeRange(events)[1]

        if todayEventTime is not None:
            closeTimeFormatted = todayEventTime.strftime('%H:%M')

            Schedule.writeEventException(Schedule.getTodayDate(), closeTimeFormatted)

        time.sleep(REFRESH_INTERVAL_SECONDS)

if __name__ == '__main__':
    main()
